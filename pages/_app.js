import '../styles/globals.css'
import 'bootstrap/dist/css/bootstrap.min.css';
// import '../public/css/bootstrap.min.css';
import { createGlobalStyle, ThemeProvider } from 'styled-components';
import styledNormalize from "styled-normalize";
import NProgress from "nprogress";
import { Router } from "next/router";


Router.events.on("routeChangeStart", () => {
  NProgress.start()});
Router.events.on("routeChangeComplete", (url) => {
  // logPageViewUrl(url);
  NProgress.done();
});
Router.events.on("routeChangeError", () => NProgress.done());


const GlobalStyle = createGlobalStyle`
  ${styledNormalize}
`
const theme = {
  colors: {
    rfRed: '#E6442C',
    rfBlack: '#282828',
    rfGrey: '#808080',
    rfLightGrey: '#f7f7f7'
  },
}

function MyApp({ Component, pageProps }) {
  return (
    <>
      {/* <GlobalStyle /> */}
      <ThemeProvider theme={theme}>
        <Component {...pageProps} />
      </ThemeProvider>
      <style jsx global>
        {`
          html {
            scroll-behavior: smooth;
          }
          #nprogress .bar {
            background-color: #E6442C;

            position: fixed;
            z-index: 1999;
            top: 0;
            left: 0;

            width: 100%;
            height: 5px;
          }
        `}
      </style>
    </>
  )
}

export default MyApp
