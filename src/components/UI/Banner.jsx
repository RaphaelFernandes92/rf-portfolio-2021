/* eslint-disable @next/next/no-img-element */
import React from "react";
import styled from 'styled-components';

const Banner = (props) => {

    if(props.bannerInfos == undefined) {
        return (
            <BannerWrapper image="/images/banner-no-experience.png">
                <h2>Oops...</h2>
            </BannerWrapper>
        )
    }else {
        return (
            <BannerWrapper image={props.bannerInfos.bannerlink}>
                <h2>{props.bannerInfos.CompanyName}</h2>
            </BannerWrapper>
        );
    }
}

export default Banner;

const BannerWrapper = styled.div`
    background-image:url(${props => props.image});
    max-width:1950px;
    height:40vh;
    background-size:cover;
    background-position:center;
    position:relative;
    margin-bottom:20px;
    h2{
        position:absolute;
        z-index:100;
        text-transform:uppercase;
        color:white;
        font-size:3.5em;
        display:block;
        text-align:center;
        width:100%;
        bottom:24px;
    }
    img {
        position:absolute;
        top:0;
        left:0;
        z-index:50;
        max-width:100%;
    }
    .placeHolder {
        max-width:100%;
        min-height:300px;
        height:500px;
        background-color:${props=> props.theme.colors.rfBlack};
    }

    @media (max-width:592px) {
        h2 {
            font-size:1.5rem;
        }
    }
    
`