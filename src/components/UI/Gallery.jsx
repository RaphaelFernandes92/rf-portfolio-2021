/* eslint-disable @next/next/no-img-element */
import React, { useEffect } from "react";
import styled from 'styled-components'

const Gallery = (props) => {
    console.log('gallery props : ', props);
   const links = props.generateLinks(props.links);

    return (
        <GalleryWrapper >
            {links}
        </GalleryWrapper>
    );
}

export default Gallery;

const GalleryWrapper = styled.div`

`