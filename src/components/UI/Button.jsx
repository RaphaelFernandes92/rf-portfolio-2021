import React from "react";
import Link from 'next/link'
import styled from 'styled-components'

const ButtonRF = (props) => {
    
    // console.log('Button props : ', props);

    return (
        <LinkWrapper 
            txtColor={props.txtColor ? props.txtColor : "white"}
            bgColor={props.bgColor ? props.bgColor : '#E6442C' }
        >
            <Link 
                href={props.link ? props.link : ""}
            >
                <a>
                    {props.children}
                </a>
            </Link>
        </LinkWrapper>
    )

}

export default ButtonRF;

const LinkWrapper = styled.div`
    a {
        color:${props => props.txtColor ? props.txtColor : "white" };
        background-color: ${props => props.bgColor ? props.bgColor : props.theme.colors.rfRed};
        font-size:1.125em;
        text-transform: uppercase;
        text-decoration:none;
        padding: 10px 30px;
    }
`