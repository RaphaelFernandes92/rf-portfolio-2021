/* eslint-disable @next/next/no-img-element */
import React from "react";
import styled from 'styled-components'

const BannerHome = (props) => {


    return (
        <BannerWrapper>
        </BannerWrapper>
    );
}

export default BannerHome;

const BannerWrapper = styled.div`
    width:100%;
    height:100%;
    position:absolute;
    top:0;
    left:0;
    z-index:0;
    background-image:url("/images/banner-home-rf.jpg");
    background-size:cover;
`