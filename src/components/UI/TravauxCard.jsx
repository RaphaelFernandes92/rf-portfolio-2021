/* eslint-disable @next/next/no-img-element */
import React from "react";
import { Row, Col } from 'reactstrap';
import styled from "styled-components";
import Button from "../UI/Button";

const TravauxCard = (props) => {

    return (
        <StyledCol xs="8" md="5" lg="3" >
            <div>
                <h3>
                    {props.title}<br />
                    <span className="ssTitre">{props.subTitle}</span>
                    <span></span>
                </h3>
                <p className="description">{props.description}</p>
                <Row>
                    <Col xs="12" sm="6" >
                        <Button
                            link={'/travaux/'+props.title}
                        > 
                            Détails
                        </Button>
                    </Col>
                    <Col xs="12" sm="6" >
                        <div className="logoDuree">
                            <p className="duree">{props.duree}</p>
                            <img src={props.logo} alt={props.alt} />
                        </div>
                    </Col>
                </Row>
            </div>
        </StyledCol>
    )
}

export default TravauxCard;

const StyledCol = styled(Col)`
    h3 {
        position:relative;
        text-transform:uppercase;
        font-family:arial, sans-serif;
        font-weight:bold;
        color:${props=> props.theme.colors.rfRed};
        
        .ssTitre {
            font-weight:normal;
            text-transform:none;
            color:${props=> props.theme.colors.rfBlack};
        }

        span:last-of-type{
            display:block;
            width:7px;
            height:54px;
            background-color:${props=> props.theme.colors.rfRed};
            position:absolute;
            top:6px;
            left:-20px;
            
        }
    }
    .description {
        color:${props=> props.theme.colors.rfGrey};
        margin-bottom:30px;
    }

    .duree {
        margin-bottom:0;
        font-size:1.1em;
        text-align:right;
        font-style:italic;
        color:${props=> props.theme.colors.rfRed};
    }
    img {
        max-width:120px;
    }

    .row {
        align-items:center;
    }
    
    .logoDuree {
        text-align:right;
        padding-bottom:10px;
    }
    
    @media (max-width: 767.98px) { 
        margin-bottom:100px;
    }

    @media (max-width: 575.98px) { 
        .description {
            margin-bottom:15px;
        }
        .row {
            flex-direction:column-reverse;
        }
        .col-12 {
            display:flex;
            justify-content:center;
        }
        .logoDuree img {
            margin-bottom: 20px;
        }

    }

`