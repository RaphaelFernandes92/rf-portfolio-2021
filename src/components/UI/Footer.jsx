/* eslint-disable @next/next/no-img-element */
import React from "react";
import { Container, Row, Col } from 'reactstrap';
import Image from 'next/image'
import Link from 'next/link';
import styled from 'styled-components'

const rfLogo = '/images/RF.png'
const mailIcon = '/images/email-footer-icon.png'
const gitIcon = '/images/git-icon.png'
const linkedinIcon = '/images/linkedin-icon.png'

const Banner = (props) => {

    return (
        <FooterWrapper fluid={true}>
            <hr />
            <Row>
                <Col xs="auto">
                <Link href="/" >
                    <a><img src={rfLogo} alt="Raphael Fernandes Logo" width="48" height="28" onClick={()=>window.scrollTo(0,0)} /></a>
                </Link>                    
                </Col>
                <Col>
                    <p>
                        © <span>Raphaël Fernandes</span> 2021-2021 <br />
                        Design et intégration : <span>Raphaël Fernandes</span>
                    </p>
                </Col>
            </Row>
            <Row>
                <Col xs="auto">
                <Link href="mailto:rfer92@gmail.com">
                    <a>
                        <img src={mailIcon} alt="Email icon : contacter Raphaël Fernandes" width={24} height={18} />
                    </a>
                </Link>
                <Link href="https://www.linkedin.com/in/raphael-fernandes-726679157/">
                    <a target="_blank">
                        <img src={linkedinIcon} alt="Raphael Fernandes linkedin profile" width={18} height={18} />
                    </a>
                </Link>
                <Link href="https://gitlab.com/RaphaelFernandes92" >
                    <a target="_blank">
                        <img src={gitIcon} alt="Raphael Fernandes github profile" width={18} height={18} />
                    </a>
                </Link>
                </Col>
            </Row>
        </FooterWrapper>
    );
}

export default Banner;

const FooterWrapper = styled(Container)`
    padding-top:30px;
    padding-bottom:30px;
    background-color:white;

    p{
        text-align:right;
        color:${props=> props.theme.colors.rfGrey};
        span {
            color:${props=> props.theme.colors.rfRed};
        }
    }
    .row:first-of-type {
        align-items:center;
    }
    .row:last-of-type .col-auto {
        margin-left:auto;
        img {
            margin-left:15px;
            cursor:pointer;
        }
    }
`