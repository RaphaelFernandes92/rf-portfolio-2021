import React from "react";
import { Col } from 'reactstrap';
import styled from "styled-components";

const CompetenceCard = (props) => {

    return (
        <StyledCol xs="8" md="5" lg="3" >
            <h3>
                {props.title}<br />
                <span>{props.subTitle}</span>
                <span></span>
            </h3>
            <p>{props.text}</p>
            <span>
                {props.signature}
            </span>
        </StyledCol>
    )
}

export default CompetenceCard;

const StyledCol = styled(Col)`
    h3 {
        position:relative;
        text-transform:uppercase;
        font-family:arial, sans-serif;
        span:last-of-type{
            display:block;
            width:7px;
            height:55px;
            background-color:white;
            position:absolute;
            top:5px;
            left:-20px;
            
        }
    }
    
    h3 span, p {
        color:white;
    }
    

    > span {
        font-style:italic;
        float:right;
    }
`