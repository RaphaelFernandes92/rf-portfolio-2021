/* eslint-disable @next/next/no-img-element */
import React from "react";
import styled from 'styled-components';
import Button from '../UI/Button';
import { useRouter } from 'next/router'

const IntroHome = () => {
    const router = useRouter();
    return (
        <StyleWrapper>

            <IntroWrapper>

                <StyledBonjour>
                    <span></span>
                    <p>Bonjour,</p>
                </StyledBonjour> 

                <h1>
                    <span>Développeur front-end</span> basé à Paris,<br /><span>à la recherche d&apos;un emploi.</span>
                </h1>
                <p>
                    &quot;Spécialisé dans l&apos;intégration et développement de projets web sous React, je suis à votre disposition pour vous aider à lancer votre idée ou rejoindre votre équipe afin d&apos;avancer ensemble et de voir le projet grandir.
                </p>
                <p>
                    Impliqué, rigoureux et force de proposition j&apos;ai hâte de travailler à vos côtés.&quot;
                </p>

                <Button
                    link={"/#travaux"}
                    txtColor={"white"}
                    bgColor={"#E6442C"}
                >
                    Voir mes travaux
                </Button>

            </IntroWrapper>

            <img 
                src="/images/arrow.png" 
                alt="arrow" 
                onClick={() => {router.push("/#competences")}}
            />

        </StyleWrapper>
    );
}


export default IntroHome;


const StyleWrapper = styled.div`
    position:relative;
    height:100vh;
    display:flex;
    align-items:center;
    background-image:url("/images/banner-home-rf.jpg");
    background-size:cover;

    img {
        position:absolute;
        z-index:500;
        margin-left:calc(50% - 33px);
        bottom:0;
        cursor:pointer;
    }
`

const IntroWrapper = styled.div`
    position:absolute;
    z-index:500;
    margin-left:10%;
    color:white;
    font-family:arial, sans-serif; 
    
    h1 {
        font-size:3em;
        margin-top:68px;
        font-family:arial, sans-serif;
        span {
            color:${props => props.theme.colors.rfRed};
        }
    }

    p {
        font-style:italic;
        font-size:1.5em;
        :last-of-type{
            margin-bottom: 60px;
        }
    }
    @media (min-width:1690px) {
        width : 50%;
    }
    @media (min-width:1350px) and (max-width:1689px) {
        width : 60%;
    }
    @media (min-width:1160px) and (max-width:1350px) {
        width : 70%;
    }
    @media (min-width:812px) and (max-width:1159px) {
        width : 95%;
        margin-left:3%;
    }
    @media (min-width:768px) and (max-width:812px) {
        padding-top:100px;
        width : 96%;
        margin-left:3%;
        margin-right:1%;
        
        h1{
            font-size:2em;
        }

        p{
            font-size:1.2em;
        }
    }
    @media (max-width:768px) {
        width : 94%;
        margin-left:4%;
        margin-right:2%;
    }
    @media (max-width: 767.98px) { 
        width:80%;
        margin-left:10%;
        margin-right:5%;
        h1{
            font-size:2em;
        }

        p{
            font-size:1.2em;
        }

    }
    @media (max-width: 575.98px) { 
        
        width:80%;
        margin-left:10%;
        margin-right:5%;
        h1{
            font-size:1.5em;
            margin-top:30px;
        }

        p{
            font-size:1em;
        }
    }

    @media (max-width: 330px) {
        h1{
            font-size:1.3em;
            margin-bottom:10px;
            margin-top:25px;
        }

        p:last-of-type{
            margin-bottom:20px;
        }
    }


`


const StyledBonjour = styled.span`
    position:relative;
    display:inline;
    color:white;

    span {
        display:block;
        width:150px;
        height:35px;
        background-color:${props => props.theme.colors.rfRed};
        position: absolute;
        left: -15px;
        top: -1px;
    }

    p {
        font-size:3em;
        position:absolute;
        top:0;
        left:0;
        font-style:normal;
    }

    
    @media (min-width: 576px) and (max-width: 767.98px) { 
        p{
            font-size:1.5em;
        }
        span {
            display:block;
            width:83px;
            height:23px;
            background-color:${props => props.theme.colors.rfRed};
            position: absolute;
            left: -11px;
            top: -2px;
        }
    }
    @media (max-width: 575.98px) { 
        p {
            font-size:1.5em;
        }
        span {
            display:block;
            width:83px;
            height:23px;
            background-color:${props => props.theme.colors.rfRed};
            position: absolute;
            left: -11px;
            top: -2px;
        }
    }

    

    @media (max-width: 330px) {
        span {
            display:block;
            width:70px;
            height:18px;
            background-color:${props => props.theme.colors.rfRed};
            position: absolute;
            left: -13px;
            top: -1px;
        }
    
        p {
            font-size:1.3em;
            position:absolute;
            top:0;
            left:0;
            font-style:normal;
        }
    }
`
