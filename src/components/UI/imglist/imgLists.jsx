export const ListOfSoftwareImages = [
    {
        src: '/images/adobe-photoshop-logo.png',
        alt: 'Logo Adobe Photoshop'
    },
    {
        src: '/images/adobe-xd-logo.png',
        alt: 'Logo Adobe XD'
    },
    {
        src: '/images/adobe-illustrator-logo.png',
        alt: 'Logo Adobe Illustrator'
    },
    {
        src: '/images/Adobe-InDesign-logo.png',
        alt: 'Logo Adobe Illustrator'
    }
];

export const ListOfLanguagesImages = [
    {
        src: '/images/HTML5-logo.png',
        alt: 'HTML5 logo'
    },
    {
        src: '/images/css3-logo.png',
        alt: 'CSS3 logo'
    },
    {
        src: '/images/Bootstrap-logo.png',
        alt: 'Bootstrap logo'
    },
    {
        src: '/images/JavaScript-logo.png',
        alt: 'JavaScript logo'
    },
    {
        src: '/images/react-logo.png',
        alt: 'React logo'
    },
    {
        src: '/images/Next.js-logo.png',
        alt: 'Next.js logo'
    },
    {
        src: '/images/node.js-logo.png',
        alt: 'Node.js logo'
    },
    {
        src: '/images/Expressjs-logo.png',
        alt: 'Express.js logo'
    }
];

