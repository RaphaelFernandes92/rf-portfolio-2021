/* eslint-disable @next/next/no-img-element */
import React from "react";
import { Row, Col } from 'reactstrap';
import styled from "styled-components";

const generateColList = (imgList) => imgList.map((img, index) => {
    return (
            <img src={img.src} alt={img.alt} key={index} />
    );
});

const ImgList = (props) => {

    return (
        <StyledRow>
            <h3>{props.title}<span></span></h3>
            {generateColList(props.list)}
        </StyledRow>
    )

}

export default ImgList;

const StyledRow = styled(Row)`
    padding-top:60px;
    display:flex;
    align-items:center;
    justify-content:space-between;
    
    h3 {
        text-transform:uppercase;
        font-size:1.9em;
        position:relative;
        margin-bottom:30px;
        color:${props=>props.theme.colors.rfBlack};
        font-family:arial, sans-serif;

        span {
            display:block;
            width:4px;
            height: 23px;
            background-color:${props=> props.theme.colors.rfRed};
            position:absolute;
            top:7px;
            left:-1px;
        }

    }    

    img {
        margin-bottom:50px;
        max-width:94px;
        height:100%;
    }

    margin: 0 15px !important;
`