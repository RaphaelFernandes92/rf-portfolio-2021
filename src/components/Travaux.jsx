import React from "react";
import styled from "styled-components";
import { Container, Row } from 'reactstrap';
import TravauxCard from "./UI/TravauxCard";

const Travaux = () => {

    return (

        <CompWrapper fluid={true}>
            <h2 id="travaux">Travaux</h2>
            <Row>
                <TravauxCard 
                    title={'Phacil.delivery'}
                    subTitle={'Développeur Front-End'}
                    description={'Intégration responsive du site phacil.delivery, développement d\'une plateforme B2B.'}
                    duree={'1 an 5 mois'}
                    alt={'Phacil.delivery logo'}
                    logo={'/images/logo-phacil-delivery.png'}
                />
                <TravauxCard 
                    title={'Tripshake'}
                    subTitle={'Développeur Full-Stack'}
                    description={'Développement du front-end et back-end de l\'application Tripshake.'}
                    duree={'2 semaines'}
                    alt={'Tripshake logo'}
                    logo={'/images/Tripshake-logo.png'}
                />
                <TravauxCard 
                    title={'Carredeboeuf.com'}
                    subTitle={'Développeur Front-End'}
                    description={'Mises à jour du site, design et intégration des campagnes emailings.'}
                    duree={'7 ans 4 mois'}
                    alt={'Carredeboeuf logo'}
                    logo={'/images/logo-carre-de-boeuf.png'}
                />
            </Row>
        </CompWrapper>

    )
}

export default Travaux;

const CompWrapper = styled(Container)`
    background-color:${props => props.theme.colors.rfLightGrey};
    padding-bottom:60px;

    h2 {
        color: ${props => props.theme.colors.rfRed};
        font-size:3.50em;
        text-align:center;
        margin-bottom:100px;
        padding-top:70px;
        font-family:arial, sans-serif;
    }

    .row {
        justify-content:space-around;
    }
    
    @media (max-width: 375px){
        h2{
            font-size:2.5em;
        }
    }
`