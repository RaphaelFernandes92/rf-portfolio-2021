/* eslint-disable @next/next/no-img-element */
import React, { useEffect, useState } from 'react';
import { useRouter } from 'next/router'
import Link from 'next/link';
import Image from 'next/image'
import styled from 'styled-components'
import {
  Collapse,
  Navbar,
  Nav,
  NavItem
} from 'reactstrap';

const rfLogo = '/images/RF.png'
const menuIcon = '/images/menu-mobile-icon.png'
const crossIcon = '/images/cross-menu.png'

const NavBar = (props) => {
  const router = useRouter();
  const [isOpen, setIsOpen] = useState(false);

  const toggle = () => {
    window.innerWidth < 768 ? setIsOpen(!isOpen) : null;
  }

  const [offset, setOffset] = useState(0);

  useEffect(() => {
    window.onscroll = () => {
      setOffset(window.pageYOffset)
    }
  }, []);

  return (
    <>
    <NavBarWrapper>
      <Navbar  light expand="md"  isscrolled={offset != 0 ? "true" : "false"} isopen={JSON.stringify(isOpen)}>
        <Link href="/"  >
            <a 
              className="logoRF" 
              onClick={()=>{
                isOpen ? toggle() : null;
                window.scrollTo(0,0);
              }}>
                <img src={rfLogo} alt="Raphael Fernandes Logo" />
            </a>
        </Link>
        <span className="menuToggler">
          <img src={ isOpen ? crossIcon : menuIcon } alt="" onClick={toggle}   />
        </span>
        <Collapse isOpen={isOpen} navbar>
          <Nav className="mr-auto" navbar>
            <NavItem>
                <Link href="/">
                    <a 
                      className={router.pathname === "/" ? "actif" : null} 
                      onClick={()=>{
                        toggle();
                        window.scrollTo(0,0);
                      }}
                    >Accueil</a>
                </Link>
            </NavItem>
            <NavItem>
                <Link href="/#travaux">
                    <a className={router.pathname.includes("/travaux/") ? "actif" : null} onClick={toggle}>Travaux</a>
                </Link>
            </NavItem>
            <NavItem>
                <Link href="/cv">
                    <a className={router.pathname === "/cv" ? "actif" : null} onClick={toggle}>CV</a>
                </Link>
            </NavItem>
            {/* <NavItem>
                <Link href="/wip">
                    <a>WIP</a>
                </Link>
            </NavItem> */}
            <NavItem>
                <Link href="/#contact">
                    <a  className={router.pathname === '/contact' ? 'actif' : 'none'} onClick={toggle}>Contact</a>
                </Link>
            </NavItem>
          </Nav>
        </Collapse>
      </Navbar>
    </NavBarWrapper>
    {isOpen === true ?
      <StyledBackdrop onClick={toggle}></StyledBackdrop>
      : null    
    }
      </>
  );
}

export default NavBar;

const StyledBackdrop = styled.div`
      background-color: ${props => props.theme.colors.rfBlack};
      opacity:0.8;
      width:100%;
      height:100%;
      position:fixed;
      z-index:999;
      cursor:pointer;
  
`

const NavBarWrapper = styled.div`
  position: fixed;
  width:100%;
  top: 0;
  z-index:1000;

  .navbar {
    padding:5px 0 5px 40px;
  }

  .navbar-nav {
    margin-left:auto;

    .nav-item {
      font-size:2.1em;

      a {
        font-family:arial;
        text-decoration:none;
        color:white;
      }
      a.actif {
        color: ${props => props.theme.colors.rfRed};
      }
      a:hover{
        color: ${props => props.theme.colors.rfRed};
      }
    }
  }

  [isscrolled="true"] {
    background-color:white;
    box-shadow: 0px 5px 7px rgba(0, 0, 0, 0.2);
    .nav-item a {
      color: ${props => props.theme.colors.rfGrey};
    }
  }

  @media (min-width: 768px) {
    .nav-item {
      a {
        padding:0 33px;
      }
      :last-of-type a{
        padding: 0 20px 0 37px;
      }
    }
    .menuToggler {
      display:none;
    }
    .backdrop {
      display:none;
    }
  }

  @media (width = 768px) {
    .navbar {
      .nav-item {
        a {
          margin-left:15px;
          :last-of-type{
            margin-right:15px;
          }
        }
      }  
    }    
  }

  @media (max-width: 768px) {
    background-color:white;

    .logoRF img {
      max-width:60px !important;
    }

    .navbar {
      
    .navbar-nav {
      padding-top:20px;
    }
    padding:7px 0 7px 20px;
      .nav-item {
        a {
          padding:0;
          color: ${props => props.theme.colors.rfGrey};
        }
      }
    }

    .navbar[isopen="true"] {
      padding-bottom:20px;

      .navbar-toggler {
        border:none;
        box-shadow:none;

        .navbar-toggler-icon {
          background-image:url(images/menu-mobile-icon.png);
        }

      }

    }

    .menuToggler {
      cursor:pointer;
      margin-right:20px;
    }
    
  }

  @media (max-width: 375px) {
    .logoRF img {
      max-width:40px !important;
    }
    .menuToggler img {
      max-width:20px;
    }
    .navbar .nav-item {
      font-size:1.5em;
    }
  }


`