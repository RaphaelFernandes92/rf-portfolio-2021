import React from "react";
import { Container } from 'reactstrap';
import ImgList from './UI/imglist/ImgList';
import { ListOfSoftwareImages, ListOfLanguagesImages} from './UI/imglist/imgLists';

const Langages = (props) => {

    return (
        <Container fluid={true}>
            <ImgList 
                title={"Design"}
                list={ListOfSoftwareImages}
            />
            <ImgList 
                title={"Intégration / développement"}
                list={ListOfLanguagesImages}
            />
        </Container>
    )
}

export default Langages;