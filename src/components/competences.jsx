import React from "react";
import styled from "styled-components";
import { Container, Row } from 'reactstrap';
import CompetenceCard from "./UI/CompetenceCard";

const Competences = () => {

    return (

        <CompWrapper fluid={true}>
            <h2 id="competences">Compétences</h2>
            <Row>
                <CompetenceCard 
                    title={"intégration"}
                    subTitle={"html/css/js"}
                    text={"Site web, emailing, application mobile, je me charge de transposer votre maquette visuelle en code lisible et compréhensible par votre navigateur."}
                    signature={"pixel perfect"}
                />
                <CompetenceCard 
                    title={"Responsive design"}
                    subTitle={"Ordinateur, tablette, mobile"}
                    text={"Votre site s'adaptera automatiquement aux différents supports afin de fournir à vos visiteurs une navigation des plus agréable."}
                    signature={"pratique"}
                />
                <CompetenceCard 
                    title={"developpement"}
                    subTitle={"React.js, React native, Node.js"}
                    text={"Une fois la maquette traduite en code, je me charge de développer les différentes fonctionnalités pour un rendu final impeccable."}
                    signature={"lisible"}
                />
            </Row>
        </CompWrapper>

    )
}

export default Competences;

const CompWrapper = styled(Container)`
    background-color:${props => props.theme.colors.rfRed};
    padding-bottom:60px;

    .row {
        justify-content:space-around;
    }

    h2 {
        color: white;
        font-size:3.50em;
        text-align:center;
        margin-bottom:100px;
        padding-top:75px;
        font-family:arial, sans-serif;
    }

    @media (min-width: 992px) { 
        .row {
            margin: 0 !important;
        }
     }

     @media (min-width: 768px) { 
        .row {
            margin: 0 15px !important;
        }
        .col-md-5 {
            margin-bottom:40px;
        }
      }

    @media (max-width: 767.98px) { 
        .row {
            margin: 0 15px !important;
        }
        .col-8 {
            margin-bottom:60px;
        }
    }

    @media (max-width: 375px){
        h2{
            font-size:2.5em;
        }
    }
`