/* eslint-disable @next/next/no-img-element */
import React, {useState} from "react";
import styled from "styled-components";
import { Container } from 'reactstrap';
import ButtonRF from "./UI/Button";

const Contact = () => {
    const [copy, setCopy] = useState(['', '']);


    const copyText = (info) => {

        if(info === "number"){
            navigator.clipboard.writeText("0663025668");
            setCopy(['Numéro copié dans le presse papier!', '']);
        }else{
            navigator.clipboard.writeText("rfer92@gmail.com");
            setCopy(['', 'Email copié dans le presse papier!']);
        }
        
    }

    return (

        <CompWrapper fluid={true}>
            <h2 id="contact">Contact</h2>
            <div>
                <div>
                    <h3>Raphaël Fernandes<span></span></h3>
                </div>
                <div className="infoNumber" 
                        onClick={()=>{
                            copyText("number");
                        }}>
                    <img src="/images/phone.png" alt="phone icon" />
                    <img src="/images/phone-number-rf.png" alt="phone number RF" />
                    { 
                    copy[0] !== '' ?
                        <p className="copiedTxt">{copy[0]}</p>
                        : null
                    }
                </div>
                <div className="infoMail" 
                        onClick={()=>{
                            copyText("email");
                        }}>
                    <img src="/images/email.png" alt="email icon" />
                    <img src="/images/contact-RF-mail.png" alt="phone number RF" />
                    { 
                    copy[1] !== '' ?
                        <p className="copiedTxt">{copy[1]}</p>
                        : null
                    }
                    {/* <ButtonRF 
                        txtColor="#E6442C"
                        bgColor="white"
                    >
                        Envoyer un email
                    </ButtonRF> */}
                </div>
            </div>


        </CompWrapper>

    )
}

export default Contact;

const CompWrapper = styled(Container)`
    background-color:${props => props.theme.colors.rfRed};
    padding-bottom:60px;
    text-align:center;
    height:100vh;

    h2 {
        color: white;
        font-size:3.50em;
        text-align:center;
        margin-bottom:60px;
        padding-top:70px;
        font-family:arial, sans-serif;
        height:10%;
    }
    
    >div {
        height:90%;
        display: flex;
        flex-direction: column;
        justify-content: space-evenly;
    }

    h3 {
        position:relative;
        text-transform:uppercase;
        color:white;
        font-size:2.8em;
        display:inline;
    }
    h3 span {
        display:block;
        width:7px;
        height:34px;
        background-color:${props=> props.theme.colors.rfBlack};
        position:absolute;
        top:15px;
        left:-17px;
    }

    .infoNumber, .infoMail {
        display: flex;
        flex-direction: column;
        align-items: center;
        cursor:pointer;

        img:first-of-type {
            margin-bottom:10px;
        }

    }
    
    .copiedTxt {
        font-style:italic;
        color:black;
        opacity:0;
        animation-duration: 3s;
        animation-name: fadeOut;
      }
      
      @keyframes fadeOut {
        from {
          opacity:1;
        }
      
        to {
          opacity:0;
        }
      }

      @media (max-width: 375px){
        h2{
            font-size:2.5em;
        }
    }
      
`