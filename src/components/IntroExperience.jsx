/* eslint-disable @next/next/no-img-element */
import React from "react";
import styled from "styled-components";
import Link from 'next/link';
import { Container, Row, Col } from 'reactstrap';


const IntroExperience = (props) => {

    console.log('introExperience props : ', props);

    if (props.infoExperience == undefined){
        return (
            <IntroWrapper fluid={true}>
            <Row>
                <Col>
                    <p>Aucune expérience avec ce nom.</p>
                    <Link href={'/'}>
                        <a>
                        Revenir à l&apos;accueil
                        </a>
                    </Link>
                </Col>
            </Row>
            </IntroWrapper>
        )
    } else {
        return (
            <IntroWrapper fluid={true}>
            <Row>
                <Col>
                    <img src={props.infoExperience.logoUrl} alt={props.infoExperience.companyName + ' logo'} />
                    <p>{props.infoExperience.period}</p>
                    <h1>{props.infoExperience.jobTitle}<span></span></h1>
                    <p className="description">{props.infoExperience.description}</p>

                    {
                        props.infoExperience.url ? 
                        <Link href={props.infoExperience.url}>
                            <a target="_blank">
                                {props.infoExperience.url}
                            </a>
                        </Link>
                        : null
                    }

                </Col>
            </Row>
            </IntroWrapper>
        )
    }
};


export default IntroExperience;


const IntroWrapper = styled(Container)`
    .col {
        display:flex;
        flex-direction:column;
        align-items:center;

        p:first-of-type{
            font-size:1em;
            text-align:right;
            font-style:italic;
            color:${props=> props.theme.colors.rfRed};
            margin-bottom:30px;
        }

        h1 {
            text-transform:uppercase;
            font-size:1.5em;
            color:${props=> props.theme.colors.rfBlack};
            position:relative;
            margin-bottom:10px;
            span {
                background-color:${props=> props.theme.colors.rfRed};
                display:block;
                position:absolute;
                top: 7px;
                width: 3px;
                height: 17px;
                left: -9px;
            }
        }

        p.description{
            font-size:1em;
            text-align:center;
            color:${props=> props.theme.colors.rfGrey};
            font-style:italic;
        }
    }
    img {
        max-width:210px;
        margin-bottom:10px;
    }    
`


