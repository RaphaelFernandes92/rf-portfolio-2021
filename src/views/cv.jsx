/* eslint-disable @next/next/no-img-element */
import React from "react";
import NavBar from "../components/menu/NavBar";
import Footer from "../components/UI/Footer";
import styled from "styled-components";
import { Document, Page } from 'react-pdf'

const CV = () => (
    <>
        <NavBar />
        <CVWrapper>
        <h2>Télécharger mon CV :</h2>
        <a href="/images/CV-Raphael-Fernandes-2021.pdf" download>
            <img src="images/PDF-file-icon.png" alt="pdf icon" width="40"/>
        </a>
        <Document 
            file="images/CV-Raphael-Fernandes-2021.pdf"
            options={{ workerSrc: "/pdf.worker.js" }}
        >
            <Page pageNumber={1} width="600" className="pdfPage" />
        </Document>
        </CVWrapper>
        <Footer />
    </>
);

export default CV;

const CVWrapper = styled.div`
    background-color:${props => props.theme.colors.rfBlack};
    width:100%;
    display:flex;
    flex-direction:column;
    align-items:center;
    justify-content:center;
    padding:100px 0 50px 0;
    color:white;

    a{
        margin-bottom:30px;
    }

    h2 {
        color:white;
    }

    @media (max-width:592px){
        height:100vh;
        .react-pdf__Document, .pdfPage {
            display:none;
        }
    }
`