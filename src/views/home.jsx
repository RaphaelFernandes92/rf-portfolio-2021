import React from "react";
import Head from 'next/head'
import NavBar from "../components/menu/NavBar";
import BannerHome from "../components/UI/BannerHome";
import IntroHome from "../components/UI/IntroHome";
import Competences from "../components/competences";
import Langages from "../components/Langages";
import Travaux from "../components/Travaux";
import Contact from "../components/Contact";
import Footer from "../components/UI/Footer";

const Home = () => {

    return (
            <>
                <Head>
                    <title>Raphaël Fernandes - Web développeur front-end React</title>
                    <meta name="description" content="Raphaël Fernandes - Web développeur front-end React, react native, js, node, express, HTML, CSS basé à Paris. Actuellement à la recherche d'un emploi" />
                    <link rel="icon" href="/raphael-fernandes-favicon.ico" />
                </Head>

                <NavBar />
                <IntroHome />
                <Competences />
                <Langages />
                <Travaux />
                <Contact />
                <Footer />
            </>
    )

}

export default Home;



