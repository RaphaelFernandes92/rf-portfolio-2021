/* eslint-disable @next/next/no-img-element */
import React, { useEffect, useState } from "react";
import axios from "axios";
import keys from "../config/config";
import NavBar from "../components/menu/NavBar";
import IntroExperience from "../components/IntroExperience";
import styled from "styled-components";
import Banner from "../components/UI/Banner";
import Footer from "../components/UI/Footer";

const Experience = (props) => {
    const [info, setInfo] = useState(props.expInfo[0]);

    // useEffect(() => {
    //     setInfo(props.expInfo[0]);
    //   }, [props.expInfo]);

    const generateLinks = (linksArray) => linksArray.map((link, key, arr)=>{
        if (arr.length - 1 !== key ){
            return (
                <>
                    <img src={link}  alt={info.companyName+" work "+key} key={key}/>
                    <hr />
                </>
            )
        } else {
            return (
                <>
                    <img src={link}  alt={info.companyName+" work "+key} key={key}/>
                </>
            )
        }
    });

    return(    
    <>
        <NavBar />

        <Banner 
            bannerInfos={info}
        />

        <IntroExperience 
            infoExperience={info}
        />

        {
        info !== undefined  && info !== null ? 
        <GalleryWrapper>
            {generateLinks(info.imgLinks)}
        </GalleryWrapper>
        : null
        }

        <Footer />
    </>
    )

};

export default Experience;

Experience.getInitialProps = async ({ query, req, res }) => {

    // console.log('Dans getInitialProps : query.travauxExp : ', query.travauxExp);

    const ExperienceName = query.travauxExp;
    const url = keys.baseUrl+'/travaux/'+ExperienceName;

    console.log('url : ', url);

    const initialProps = {
        expInfo: []
    };

    await axios.get(url)
    .then(response => {
        if(response.data.success == true){
            // console.log('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ response.data.experiences[0] : ', response.data.experience[0]);
            initialProps.expInfo.push(response.data.experience[0]);
        }
    }, error => {
        console.log('Err dans la requete : ', error);
    });

    return { ...initialProps };

  }

const GalleryWrapper = styled.div`
background-color:${props=> props.theme.colors.rfLightGrey};
padding:50px 0;
margin-top:40px;
    hr{
        width:90%;
        margin:20px 5%;
    }
    img {
        max-width:90%;
    }
    text-align:center;
`


